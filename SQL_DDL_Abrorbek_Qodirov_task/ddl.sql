
CREATE DATABASE my_database;


CREATE SCHEMA IF NOT EXISTS my_schema;

SET search_path TO my_schema;

CREATE TABLE IF NOT EXISTS Student (
    student_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    gender CHAR(1) CHECK (gender IN ('M', 'F')) NOT NULL,
    date_of_birth DATE CHECK (date_of_birth > '2000-01-01') NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);


CREATE TABLE IF NOT EXISTS Course (
    course_id SERIAL PRIMARY KEY,
    course_name VARCHAR(100) NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);


CREATE TABLE IF NOT EXISTS Enrollment (
    enrollment_id SERIAL PRIMARY KEY,
    student_id INT REFERENCES Student(student_id),
    course_id INT REFERENCES Course(course_id),
    enrollment_date DATE DEFAULT CURRENT_DATE NOT NULL,
    grade DECIMAL(3, 2) CHECK (grade >= 0) NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);


INSERT INTO Student (first_name, last_name, gender, date_of_birth)
VALUES
    ('John', 'Doe', 'M', '2000-02-15'),
    ('Jane', 'Smith', 'F', '1999-04-10');


INSERT INTO Course (course_name)
VALUES
    ('Math 101'),
    ('History 101');


INSERT INTO Enrollment (student_id, course_id, grade)
VALUES
    (1, 1, 3.5),
    (1, 2, 4.0),
    (2, 1, 3.0);

UPDATE Student SET record_ts = DEFAULT;
UPDATE Course SET record_ts = DEFAULT;
UPDATE Enrollment SET record_ts = DEFAULT;

